create schema if not exists My_virtual_store default character set utf8;
use My_virtual_store;
create table if not exists My_virtual_store.Tbl_productos(
	id_producto int not null auto_increment,
    codigo_producto varchar(100) not null,
    descripcion_producto varchar(200) not null,
    precio_compra double not null,
    precio_venta double not null,
    fecha_vencimiento date null,
    unidad_medida varchar(45) not null,
    fecha_registro timestamp not null,
    id_proveedor int not null,
    id_cliente int not null,
    id_categoria int not null,
    cantidad int null,
    estado bit(2) not null,
    Tbl_Clientes_id_cliente int not null,
    Tbl_Categorias_id_categoria int not null,
    primary key (id_producto)
    );

create table if not exists My_virtual_store.tbl_proveedores(
	id_proveedor int not null auto_increment,
    nit_proveedor int not null,
    nombre_razon_social varchar(45) not null,
    direccion_proveedor varchar(45) not null,
    telefono_proveedor varchar(45) not null,
    correo_proveedor varchar(45) not null,
    estado_proveedor varchar(45) not null,
    primary key (id_proveedor)
    );
    
alter table Tbl_productos add constraint fk_Tbl_productos_Tbl_proveedores foreign key (id_proveedor) references Tbl_proveedores (id_proveedor);